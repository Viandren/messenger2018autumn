/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.messenger2018autumn.controllers;

import com.progmatic.messenger2018autumn.domain.Message;
import com.progmatic.messenger2018autumn.services.MessageService;
import com.progmatic.messenger2018autumn.utils.DateConstants;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author peti
 */
@Controller
public class MessageController {
    
    private MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }
    
    
    @RequestMapping(path = "/messagetable")
    public String showMessages(
            @RequestParam(name = "id", required = false) Long id,
            @RequestParam(name = "author", required = false) String author,
            @RequestParam(name = "text", required = false) String text,
            @RequestParam(name = "from", required = false) @DateTimeFormat(pattern = DateConstants.DATE_PATTERN) LocalDate from,
            @RequestParam(name = "to", required = false) @DateTimeFormat(pattern = DateConstants.DATE_PATTERN) LocalDate to,
            @RequestParam(name = "orderBy", required = false) String orderBy,
            @RequestParam(name = "isAsc", required = false) String isAsc,
            Model model){
        model.addAttribute("messages", messageService.findMessages(id, author, text, 
                from == null ? null : from.atStartOfDay(), 
                to == null ? null : to.atStartOfDay(), 
                orderBy, isAsc != null && isAsc.equals("asc")));
        return "messageList";
    }
    
    @RequestMapping(path = "/messages/{messageId}")
    public String showOneMessages(
            @PathVariable("messageId") Long msgId,
            Model model){
        model.addAttribute("message", messageService.findMessageById(msgId));
        return "oneMessage";
    }
    
    @GetMapping(path = "/showcreate")
    public String showCreateMessage(@ModelAttribute("messg") Message m){
        return "createMessage";
    }
    
    @PostMapping(path = "/createmessage")
    public String createMessage(@Valid @ModelAttribute("messg") Message m, BindingResult br){
        if(br.hasErrors()){
            return "createMessage";
        }
        messageService.createMessage(m);
        return "redirect:/messagetable?orderBy=createDate&isAsc=desc";
    }
}
